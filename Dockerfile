FROM docker:1.12.3-dind

RUN apk add --update bash && rm -rf /var/cache/apk/*

VOLUME /var/run/docker.sock:/var/run/docker.sock

CMD []